package ru.edu.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class CatalogTest {

    private ObjectMapper mapper;

    @Test
    public void test() {
        XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);


        Catalog catalog = null;
        try (FileInputStream fis = new FileInputStream("./input/cd_catalog.xml")) {
            catalog = mapper.readValue(fis, Catalog.class);

            assertNotNull(catalog);


        } catch (Exception e) {
            e.printStackTrace();
        }

        exampleStreams(catalog, false);

    }

    private void exampleStreams(Catalog catalog, boolean isFilter) {
        Stream<CD> stream = catalog.cdList.stream();

        if (isFilter) {
            stream = stream.filter((cd) -> cd.name.startsWith("A"));
        }

        mapper = new ObjectMapper();

        List<Album> albums = stream
                .map(cd -> new Album(cd.name, cd.year)).collect(Collectors.toList());

        List<Object> collect = albums.stream()
                .map(this::toJson)
                .map(AlbumReader::new)
                .map(AlbumReader::getAlbum)
                .map(a -> a.getName().split(""))
                .flatMap(Arrays::stream)
                .collect(Collectors.toList());

        long maxYear = albums.stream()
                .map(Album::getYear)
                .reduce(0L, Long::max);

        long minYear = albums.stream()
                .map(Album::getYear)
                .reduce(0L, Long::min);

        //эквивалентно реализации на стримах
        long minYearOldWay = 0L;
        for (Album album : albums) {
            minYearOldWay = Math.min(minYearOldWay, album.getYear());
        }


        Map<Long, List<Album>> groupByYear = new TreeMap<>(albums.stream().collect(Collectors.groupingBy(Album::getYear)));


        System.out.println(collect);
    }

    @Test
    public void exmapleFromLecture(){
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 6; ++i) {
            list.add(i);
        }

        Map<Integer, List<Integer>> map = list.stream()
                .collect(Collectors.groupingBy((Integer digit) -> {

                    return digit % 3;
                }));



        System.out.println(list);
        System.out.println(map);

    }

    public static class AlbumReader {
        private Album album;

        public AlbumReader(String json) {
            try {
                this.album = new ObjectMapper().readValue(json, Album.class);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }

        public Album getAlbum() {
            return album;
        }
    }

    private String toJson(Album obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private void exampleOld(Catalog catalog, boolean isFilter) {
        List<CD> collect = new LinkedList<>();

        catalog.cdList.forEach((cd) -> {
            //filter stage
            if (isFilter) {
                if (!cd.name.startsWith("A")) {
                    return;
                }
            }
            collect.add(cd);
        });

        List<Album> artists = new LinkedList<>();
        //map stage
        collect.forEach(cd -> {
            artists.add(new Album(cd.name, cd.year));
        });


        System.out.println(collect);
    }

}