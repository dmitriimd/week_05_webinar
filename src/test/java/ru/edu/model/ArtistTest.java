package ru.edu.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import ru.edu.MapperUtils;

import java.io.IOException;

import static org.junit.Assert.*;

public class ArtistTest {

    @Test
    public void getName() throws IOException {
        Artist artist = MapperUtils.readJson("./src/test/resources/example_files/artist.json", Artist.class);

        String jsonObject = new ObjectMapper().writeValueAsString(artist);


        assertEquals("{\"name\":\"ArtistName\",\"somthing\":\"asdasd\"}", jsonObject);
        assertEquals("ArtistName", artist.getName());
    }
}