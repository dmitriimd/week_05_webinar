package week4.screen;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import week4.Competition;
import week4.model.Athlete;
import week4.model.Participant;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class RegisterScreenTest {

    @Mock
    private Competition competition;

    @Mock
    private Scanner scanner;

    @Mock
    private PrintStream outputStream;

    private Screen screen;

    @Before
    public void setup(){
        openMocks(this);
        screen = new RegisterScreen(scanner, outputStream);
    }

    @Test
    public void readInput() {
        when(scanner.nextLine()).thenReturn("имя", "фамилия", "страна");

        Answer<Participant> participantAnswer = this::participantMockInvocation;



        when(competition.register(any(Athlete.class))).thenAnswer(participantAnswer);
        
        for (int i = 1; i <= 3; i++) {
            screen.prompt();
            verify(outputStream, times(i)).println(anyString());

            screen.readInput(competition);
            verify(scanner, times(i)).nextLine();
            if(i == 3){
                verify(outputStream, times(4)).println(anyString());
            }
        }
        
        
        //как мокать лист

        when(competition.getResults()).thenReturn(getParticipantList());
        

    }

    private Participant participantMockInvocation(InvocationOnMock invocation) {
        Athlete athlete = invocation.getArgument(0);

        assertEquals("имя", athlete.getFirstName());
        //проверяем остальные поля


        return RegisterScreenTest.this.getMockedParticipant(1L, 2L, "firstName");

    }


    private List<Participant> getParticipantList() {
        List<Participant> participants = new ArrayList<>();
        participants.add(getMockedParticipant(1L, 4L, "firstName"));
        participants.add(getMockedParticipant(2L, 2L, "firstName1"));
        participants.add(getMockedParticipant(3L, 1L, "firstName2"));
        return participants;
    }

    private Participant getMockedParticipant(long id, long score, String firstName) {
        Participant mock = mock(Participant.class);
        when(mock.getId()).thenReturn(id);
        when(mock.getScore()).thenReturn(score);
        Athlete athleteMock = mock(Athlete.class);
        when(athleteMock.getFirstName()).thenReturn(firstName);
        when(mock.getAthlete()).thenReturn(athleteMock);
        return mock;
    }
}