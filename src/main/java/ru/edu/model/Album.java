package ru.edu.model;

public class Album {

    private String name;

    private long year;

    public Album(String name, long year) {
        this.name = name;
        this.year = year;
    }

    public Album() {
    }

    public String getName() {
        return name;
    }

    public long getYear() {
        return year;
    }
}
