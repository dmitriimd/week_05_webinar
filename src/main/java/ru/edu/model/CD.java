package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CD {

    @JacksonXmlProperty(localName = "TITLE")
    String title;

    @JacksonXmlProperty(localName = "ARTIST")
    String name;

    @JsonIgnore(value = false)
    @JacksonXmlProperty(localName = "YEAR")
    int year;
}
