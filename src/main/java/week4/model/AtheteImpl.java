package week4.model;

import java.util.Objects;

public class AtheteImpl implements Athlete {

    private String firstname;
    private String lastName;
    private String country;

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return firstname;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }

    public static class Builder {
        private AtheteImpl newAthlete = new AtheteImpl();

        public Builder setFirstName(String input) {
            newAthlete.firstname = input;
            return this;
        }

        public Builder setLastName(String input) {
            newAthlete.lastName = input;
            return this;
        }

        public Builder setCountry(String input) {
            newAthlete.country = input;
            return this;
        }

        public AtheteImpl build() {
            if ((newAthlete.firstname.equals("")) ||
                    (newAthlete.lastName.equals("")) ||
                    (newAthlete.country.equals(""))) {
                throw new IllegalArgumentException("All of Athlete attributes must be entered");
            }
            return newAthlete;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AtheteImpl athelte = (AtheteImpl) o;
        return firstname.equals(athelte.firstname) &&
                lastName.equals(athelte.lastName) && country.equals(athelte.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstname, lastName, country);
    }

    @Override
    public String toString() {
        return "{" +
                "firstname='" + firstname + '\'' +
                ", lastName='" + lastName + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}

