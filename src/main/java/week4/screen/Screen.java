package week4.screen;

import week4.Competition;

public interface Screen {

    /**

     * Выводит текст на экран и подсказывает какой ввод ожидается.

     */

    void prompt();

    /**

     * Считаем пользовательский ввод, если требуется.

     * Выполним действия.

     * Вернем this если остаемся на этом экране, null если возвращаемся назад.

     *

     * @param competition class

     * @return Screen

     */

    Screen readInput(Competition competition);

}
